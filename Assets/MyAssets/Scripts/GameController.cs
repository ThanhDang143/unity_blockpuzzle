﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.IO;

public class GameController : MonoBehaviour
{
    public static GameController ins;
    public GameObject baseBGBlock;
    public GameObject baseBlock;
    public Transform map;
    public Transform blockInMap;
    public Transform spawner;
    public Transform frameBackup;
    public GameObject[] groupBlock;
    public Sprite grayBlock;
    public int rows;
    public int columns;
    public bool canRotate;
    public int score;
    public int rotateCoin;
    private Text txtScoreInOver;
    private Text txtHighScoreInOver;
    private Slider sliderTreasure;
    private ParticleSystem chestEffect;
    private Image imgChest;
    private ParticleSystem coinExplosion;
    private Sprite[] spriteChest;
    private Text txtScore;
    private Text txtHighScore;
    private Text txtRotateCoin;
    public bool isStop;
    public bool canTap;
    public int[] groupBlockID;
    [HideInInspector] public List<int> countC;
    [HideInInspector] public List<int> countR;
    [HideInInspector] public float distance;
    [HideInInspector] public float startX;
    [HideInInspector] public float startY;
    [HideInInspector] public Transform[,] mapMatrix;
    [HideInInspector] public Transform[,] chosenMatrix;
    [HideInInspector] public Transform[,] tempMatrix;

    void Awake()
    {
        ins = this;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        txtScore = UIManager.ins.gameplay.txtScore;
        txtHighScore = UIManager.ins.gameplay.txtHighScore;
        txtRotateCoin = UIManager.ins.gameplay.txtRotateCoin;
        chestEffect = UIManager.ins.gameplay.chestEffect;
        imgChest = UIManager.ins.gameplay.imgChest;
        spriteChest = UIManager.ins.gameplay.spriteChest;
        sliderTreasure = UIManager.ins.gameplay.sliderTreasure;
        txtScoreInOver = UIManager.ins.gameover.txtScore;
        txtHighScoreInOver = UIManager.ins.gameover.txtHighScore;
        coinExplosion = UIManager.ins.gameplay.coinExplosion;
        mapMatrix = new Transform[rows, columns];
        chosenMatrix = new Transform[rows, columns];
        tempMatrix = new Transform[rows, columns];
        CreateMap();
        LoadData();
    }

    private void Start()
    {
        isStop = true;
        canTap = true;
        CheckSetting();
        SetIdGroupBlock();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            StartCoroutine(IEUpTreasure(125));
        }
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            PlayerPrefs.DeleteAll();
            File.Delete(PathSave.PathMatrix);
            Debug.Log("Deleted SaveGame");
        }
    }

    public void SetIdGroupBlock()
    {
        for (int i = 0; i < groupBlock.Length; i++)
        {
            groupBlock[i].GetComponent<GroupBlock>().id = i + 1;
        }
    }

    public void CheckSetting()
    {
        // Check sound setting
        if (PlayerPrefs.HasKey(SaveKey.sound))
        {
            UIManager.ins.pause.btnSound.sprite = UIManager.ins.pause.spriteOnOff[PlayerPrefs.GetInt(SaveKey.sound)];
            UIManager.ins.setting.btnSound.sprite = UIManager.ins.setting.spriteOnOff[PlayerPrefs.GetInt(SaveKey.sound)];
        }
        else
        {
            PlayerPrefs.SetInt(SaveKey.sound, 1);
            UIManager.ins.pause.btnSound.sprite = UIManager.ins.pause.spriteOnOff[PlayerPrefs.GetInt(SaveKey.sound)];
            UIManager.ins.setting.btnSound.sprite = UIManager.ins.setting.spriteOnOff[PlayerPrefs.GetInt(SaveKey.sound)];
        }

        try
        {
            foreach (AudioSource s in AudioManager.ins.audioSourceOncePlay)
            {
                s.volume = PlayerPrefs.GetInt(SaveKey.sound);
            }
            AudioManager.ins.sourceBackGround.volume = PlayerPrefs.GetInt(SaveKey.sound);
            AudioManager.ins.sourceClick.volume = PlayerPrefs.GetInt(SaveKey.sound);
        }
        catch (System.Exception)
        {

        }

        // Check vibrate setting
        if (PlayerPrefs.HasKey(SaveKey.vibrate))
        {
            UIManager.ins.pause.btnVibrate.sprite = UIManager.ins.pause.spriteOnOff[PlayerPrefs.GetInt(SaveKey.vibrate)];
            UIManager.ins.setting.btnVibrate.sprite = UIManager.ins.setting.spriteOnOff[PlayerPrefs.GetInt(SaveKey.vibrate)];
        }
        else
        {
            PlayerPrefs.SetInt(SaveKey.vibrate, 1);
            UIManager.ins.pause.btnVibrate.sprite = UIManager.ins.pause.spriteOnOff[PlayerPrefs.GetInt(SaveKey.vibrate)];
            UIManager.ins.setting.btnVibrate.sprite = UIManager.ins.setting.spriteOnOff[PlayerPrefs.GetInt(SaveKey.vibrate)];
        }
    }

    public void PlayCoinExplosion(int coin, Vector3 exploPos)
    {
        coinExplosion.transform.position = exploPos;
        coinExplosion.Play();
        rotateCoin += coin;
        txtRotateCoin.text = rotateCoin.ToString();
    }

    public void SetTreasure(int value)
    {
        sliderTreasure.maxValue = value;
        sliderTreasure.value = 0;
    }

    public void EndGame()
    {
        StartCoroutine(IEIsLoser());
    }

    public IEnumerator IEIsLoser()
    {
        yield return new WaitForSeconds(0.5f);
        // Check các khối trong frameSpawner
        for (int i = 0; i < spawner.childCount; i++)
        {
            if (!GroupBlockCanNotDrop(spawner, i))
            {
                yield break;
            }
        }
        // Check các khối trong frameBackup
        if (frameBackup.childCount > 0)
        {
            if (!GroupBlockCanNotDrop(frameBackup, 0))
            {
                yield break;
            }
        }
        // Nếu frameBackup còn trống
        if (frameBackup.childCount == 0 && spawner.childCount <= 1)
        {
            yield break;
        }
        // End
        isStop = true;
        AudioManager.ins.PlayOther(NameSound.lose);
        Vibrate();
        //Change color block in map
        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                if (chosenMatrix[r, c] != null)
                {
                    chosenMatrix[r, c].GetComponent<SpriteRenderer>().sprite = grayBlock;
                    yield return null;
                }
            }
        }
        yield return new WaitForSeconds(2f);
        // Destroy group block in spawner
        foreach (Transform child in spawner)
        {
            child.transform.DOScale(new Vector3(0.45f, 0.45f, 0.45f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                child.transform.DOScale(new Vector3(0, 0, 0), 0.2f).SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    Destroy(child.gameObject);
                });
            });
        }
        // Destroy group block in backup
        foreach (Transform child in frameBackup)
        {
            child.transform.DOScale(new Vector3(0.45f, 0.45f, 0.45f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                child.transform.DOScale(new Vector3(0, 0, 0), 0.2f).SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    Destroy(child.gameObject);
                });
            });
        }
        // Detroy block in map
        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < columns; c++)
            {
                if (chosenMatrix[r, c] != null)
                {
                    DestroyBlockEffect(chosenMatrix[r, c].gameObject, 0.35f);
                    yield return null;
                }
            }
        }
        score = 0;
        UIManager.ins.gameplay.txtScore.text = score.ToString();
        txtScoreInOver.text = PlayerPrefs.GetInt(SaveKey.score).ToString();
        txtHighScoreInOver.text = PlayerPrefs.GetInt(SaveKey.highScore).ToString();
        GameController.ins.SetTreasure(100);
        ManagerAds.Ins.ShowInterstitial();
        UIManager.ins.ShowPopup(MenuPopup.GameOver);
        yield return new WaitForSeconds(0.5f);
        SaveGame();
    }

    public void DestroyEmptyGroupBlock()
    {
        foreach (Transform child in blockInMap)
        {
            if (child.childCount <= 0)
            {
                Destroy(child.gameObject);
            }
        }
    }

    public bool GroupBlockCanNotDrop(Transform parent, int childIndex)
    {
        GameObject checker = Instantiate(parent.GetChild(childIndex).gameObject, new Vector3(-20, -20, -20), parent.GetChild(childIndex).rotation);
        checker.transform.localScale = Vector3.one;
        for (int r = 0; r < GameController.ins.rows; r++)
        {
            for (int c = 0; c < GameController.ins.columns; c++)
            {
                for (int i = 1; i < 5; i++)
                {
                    ChooseCheckPos(i, checker.transform, r, c);
                    if (checker.GetComponent<GroupBlock>().CanDrop())
                    {
                        Destroy(checker);
                        return false;
                    }
                }
            }
        }
        Destroy(checker);
        return true;
    }

    public void ChooseCheckPos(int mode, Transform checker, int r, int c)
    {
        if (mode == 1)
        {
            checker.position = mapMatrix[r, c].position + new Vector3(0.01f, 0.01f, 0);
            return;
        }
        if (mode == 2)
        {
            checker.position = mapMatrix[r, c].position + new Vector3(-0.01f, 0.01f, 0);
            return;
        }
        if (mode == 3)
        {
            checker.position = mapMatrix[r, c].position + new Vector3(0.01f, -0.01f, 0);
            return;
        }
        if (mode == 4)
        {
            checker.position = mapMatrix[r, c].position + new Vector3(-0.01f, -0.01f, 0);
            return;
        }
    }

    private void CreateMap()
    {
        distance = baseBGBlock.GetComponent<SpriteRenderer>().size.x + 0.02f;
        startX = (-distance * (GameController.ins.columns - 1) / 2) + map.position.x;
        startY = (-distance * (GameController.ins.rows - 1) / 2) + map.position.y;

        for (int r = 0; r < GameController.ins.rows; r++)
        {
            for (int c = 0; c < GameController.ins.columns; c++)
            {
                GameObject b = Instantiate(baseBGBlock, new Vector3(startX + (distance * c), startY + (distance * r), 0), Quaternion.identity, map);
                b.name = "Map" + r + c;
                b.GetComponent<SpriteRenderer>().sortingOrder = -8;
                mapMatrix[r, c] = b.transform;
            }
        }
    }

    public void SpawnBlock()
    {
        Quaternion[] rotation = new Quaternion[4] { Quaternion.Euler(0, 0, 0), Quaternion.Euler(0, 0, 90), Quaternion.Euler(0, 0, 180), Quaternion.Euler(0, 0, 270) };
        if (spawner.childCount <= 0)
        {
            for (int i = 0; i < 3; i++)
            {
                int randomRotation = Random.Range(0, rotation.Length);
                GameObject groupBlock035;
                if (score <= 100)
                {
                    groupBlock035 = groupBlock[Random.Range(0, 2)];
                }
                else
                {
                    groupBlock035 = groupBlock[ChoseGroupBlock()];
                }
                groupBlock035.transform.localScale = Vector3.zero;
                GameObject block = Instantiate(groupBlock035, spawner.position + new Vector3(-1.35f + i * 1.35f, 0, 0), rotation[randomRotation], spawner);
                foreach (Transform bb in block.transform)
                {
                    bb.rotation = Quaternion.Euler(0, 0, 0);
                }
                block.transform.DOScale(Vector3.zero, 0).SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    block.transform.DOScale(new Vector3(0.45f, 0.45f, 0.45f), 0.2f).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        block.transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 0.3f).SetEase(Ease.InOutSine);
                    });
                });
                block.GetComponent<GroupBlock>().initialPos = spawner.position + new Vector3(-1.35f + i * 1.35f, 0, 0);
            }
        }
    }

    public int ChoseGroupBlock()
    {
        int ran = groupBlockID[Random.Range(0, groupBlockID.Length)];
        return ran;
    }

    public void UpScore(int x)
    {
        StartCoroutine(IEShowScore(x));
        StartCoroutine(IEUpTreasure(x));
    }

    public IEnumerator IEShowScore(int x)
    {
        int tempScore = score;
        score += x;
        while (tempScore < score)
        {
            tempScore += 1;
            txtScore.text = tempScore.ToString();
            if (tempScore > PlayerPrefs.GetInt(SaveKey.highScore))
            {

                PlayerPrefs.SetInt(SaveKey.highScore, tempScore);
                txtHighScore.text = PlayerPrefs.GetInt(SaveKey.highScore).ToString();
            }
            yield return null;
        }
    }

    public IEnumerator IEUpTreasure(int value)
    {
        int temp = 0;
        while (temp < value)
        {
            sliderTreasure.value += 1;
            temp += 1;
            if (sliderTreasure.value >= 0.75 * sliderTreasure.maxValue && chestEffect.isStopped)
            {
                chestEffect.Play();
                imgChest.sprite = spriteChest[1];
                imgChest.SetNativeSize();
            }

            if (sliderTreasure.value >= sliderTreasure.maxValue)
            {
                SetTreasure(Mathf.RoundToInt(sliderTreasure.maxValue) + 100);
                AudioManager.ins.PlayOther(NameSound.GotTreasure);
                PlayCoinExplosion(5, imgChest.transform.position);
                Vibrate();
                txtRotateCoin.text = rotateCoin.ToString();
                if (sliderTreasure.value < 0.75 * sliderTreasure.maxValue && chestEffect.isPlaying)
                {
                    chestEffect.Stop();
                    imgChest.sprite = spriteChest[0];
                    imgChest.SetNativeSize();
                }
                // do something
            }
            yield return null;
        }
    }

    public void DestroyBlockEffect(GameObject block, float time)
    {
        block.transform.DOScale(new Vector3(0f, 0f, 0f), time).SetEase(Ease.InOutSine).OnComplete(() =>
        {
            Destroy(block);
        });
    }

    public void CheckColumn(int c)
    {
        for (int i = 0; i < rows; i++)
        {
            if (chosenMatrix[i, c] == null && tempMatrix[i, c] == null)
            {
                return;
            }
        }
        //Full Column
        AudioManager.ins.PlayOther(NameSound.BonusNormal);
        if (!countC.Contains(c))
        {
            countC.Add(c);
        }
        for (int i = 0; i < rows; i++)
        {
            tempMatrix[i, c] = chosenMatrix[i, c];
            chosenMatrix[i, c] = null;
        }
        for (int i = 0; i < rows; i++)
        {
            if (tempMatrix[i, c] != null)
            {
                DestroyBlockEffect(tempMatrix[i, c].gameObject, 0.35f);
            }
        }
    }

    public void CheckRow(int r)
    {
        for (int i = 0; i < columns; i++)
        {
            if (chosenMatrix[r, i] == null && tempMatrix[r, i] == null)
            {
                return;
            }
        }
        //Full Row
        AudioManager.ins.PlayOther(NameSound.BonusNormal);
        if (!countR.Contains(r))
        {
            countR.Add(r);
        }
        for (int i = 0; i < columns; i++)
        {
            tempMatrix[r, i] = chosenMatrix[r, i];
            chosenMatrix[r, i] = null;
        }
        for (int i = 0; i < columns; i++)
        {
            if (tempMatrix[r, i] != null)
            {
                DestroyBlockEffect(tempMatrix[r, i].gameObject, 0.35f);
            }
        }
    }

    public void Bonus()
    {
        int combo = countR.Count + countC.Count;
        if (combo == 1)
        {
            UpScore(10 * combo + 0);
        }
        if (combo == 2)
        {
            UpScore(10 * combo + 5);
        }
        if (combo == 3)
        {
            UpScore(10 * combo + 10);
            PlayCoinExplosion(1, txtRotateCoin.transform.position);
            Vibrate();
        }
        if (combo == 4)
        {
            UpScore(10 * combo + 20);
            PlayCoinExplosion(2, txtRotateCoin.transform.position);
            Vibrate();
        }
        if (combo == 5)
        {
            UpScore(10 * combo + 50);
            PlayCoinExplosion(3, txtRotateCoin.transform.position);
            Vibrate();
        }
        if (combo == 6)
        {
            UpScore(10 * combo + 100);
            PlayCoinExplosion(5, txtRotateCoin.transform.position);
            Vibrate();
        }
        combo = 0;
        SaveGame();
    }

    public void ClearCountList()
    {
        countC.Clear();
        countR.Clear();
    }

    public void Vibrate()
    {
        if (PlayerPrefs.GetInt(SaveKey.vibrate) == 1)
        {
            Handheld.Vibrate();
        }
    }

    public void SpawnGroupBlockAgain()
    {
        if (!PlayerPrefs.HasKey(SaveKey.spawn + "0")) return;

        //Spawn in spawner
        for (int i = 0; i < 3; i++)
        {
            if (PlayerPrefs.GetInt(SaveKey.spawn + i) > 0)
            {
                GameObject groupBlock035 = groupBlock[PlayerPrefs.GetInt(SaveKey.spawn + i) - 1];
                groupBlock035.transform.localScale = Vector3.zero;
                if (PlayerPrefs.GetFloat(SaveKey.spawnAngle + i) != 0)
                {
                    GameObject block = Instantiate(groupBlock035, spawner.position + new Vector3(-1.35f + i * 1.35f, 0, 0), Quaternion.Euler(0, 0, PlayerPrefs.GetFloat(SaveKey.spawnAngle + i)), spawner);
                    foreach (Transform bb in block.transform)
                    {
                        bb.rotation = Quaternion.Euler(0, 0, 0);
                    }
                    block.transform.DOScale(Vector3.zero, 0).SetEase(Ease.InOutSine).OnComplete(() =>
                        {
                            block.transform.DOScale(new Vector3(0.45f, 0.45f, 0.45f), 0.2f).SetEase(Ease.InOutSine).OnComplete(() =>
                            {
                                block.transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 0.3f).SetEase(Ease.InOutSine);
                            });
                        });
                    block.GetComponent<GroupBlock>().initialPos = spawner.position + new Vector3(-1.35f + i * 1.35f, 0, 0);
                }
                else
                {
                    GameObject block = Instantiate(groupBlock035, spawner.position + new Vector3(-1.35f + i * 1.35f, 0, 0), Quaternion.identity, spawner);
                    block.transform.DOScale(Vector3.zero, 0).SetEase(Ease.InOutSine).OnComplete(() =>
                        {
                            block.transform.DOScale(new Vector3(0.45f, 0.45f, 0.45f), 0.2f).SetEase(Ease.InOutSine).OnComplete(() =>
                            {
                                block.transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 0.3f).SetEase(Ease.InOutSine);
                            });
                        });
                    block.GetComponent<GroupBlock>().initialPos = spawner.position + new Vector3(-1.35f + i * 1.35f, 0, 0);
                }
            }
        }

        // Spawn in backup
        if (!PlayerPrefs.HasKey(SaveKey.backup) || PlayerPrefs.GetInt(SaveKey.backup) <= 0) return;

        GameObject groupBlock0 = groupBlock[PlayerPrefs.GetInt(SaveKey.backup) - 1];
        groupBlock0.transform.localScale = Vector3.zero;
        if (PlayerPrefs.GetFloat(SaveKey.backupAngle) != 0)
        {
            GameObject block = Instantiate(groupBlock0, frameBackup.position, Quaternion.Euler(0, 0, PlayerPrefs.GetFloat(SaveKey.backupAngle)), frameBackup);
            foreach (Transform bb in block.transform)
            {
                bb.rotation = Quaternion.Euler(0, 0, 0);
            }
            block.transform.DOScale(Vector3.zero, 0).SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    block.transform.DOScale(new Vector3(0.45f, 0.45f, 0.45f), 0.2f).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        block.transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 0.3f).SetEase(Ease.InOutSine);
                    });
                });
            block.GetComponent<GroupBlock>().initialPos = frameBackup.position;
        }
        else
        {
            GameObject block = Instantiate(groupBlock0, frameBackup.position, Quaternion.identity, frameBackup);
            block.transform.DOScale(Vector3.zero, 0).SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    block.transform.DOScale(new Vector3(0.45f, 0.45f, 0.45f), 0.2f).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        block.transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 0.3f).SetEase(Ease.InOutSine);
                    });
                });
            block.GetComponent<GroupBlock>().initialPos = frameBackup.position;
        }

    }

    public void SaveBlockInSpawner()
    {
        for (int i = 0; i < 3; i++)
        {
            try
            {
                PlayerPrefs.SetInt(SaveKey.spawn + i, spawner.GetChild(i).GetComponent<GroupBlock>().id);
                PlayerPrefs.SetFloat(SaveKey.spawnAngle + i, spawner.GetChild(i).transform.eulerAngles.z);
            }
            catch (System.Exception)
            {
                PlayerPrefs.SetInt(SaveKey.spawn + i, 0);
                PlayerPrefs.SetFloat(SaveKey.spawnAngle + i, 0);
            }
        }

    }

    public void SaveBlockInBackUp()
    {
        if (frameBackup.childCount != 0)
        {
            int x = frameBackup.GetChild(0).GetComponent<GroupBlock>().id;
            PlayerPrefs.SetInt(SaveKey.backup, x);
            PlayerPrefs.SetFloat(SaveKey.backupAngle, frameBackup.GetChild(0).transform.eulerAngles.z);
        }
        else
        {
            PlayerPrefs.SetInt(SaveKey.backup, 0);
        }
    }

    public void SaveGame()
    {
        SaveMatrix s = new SaveMatrix(this);
        SaveSystem.SaveData(s, PathSave.PathMatrix);
        PlayerPrefs.SetInt(SaveKey.score, score);
        PlayerPrefs.SetInt(SaveKey.rotateCoin, rotateCoin);
        PlayerPrefs.SetFloat(SaveKey.currentTreasure, sliderTreasure.value);
        PlayerPrefs.SetFloat(SaveKey.maxTreasure, sliderTreasure.maxValue);
        SaveBlockInBackUp();
        SaveBlockInSpawner();
    }

    public void LoadData()
    {
        //Load map
        if (File.Exists(PathSave.PathMatrix))
        {
            SaveMatrix data = SaveSystem.LoadData<SaveMatrix>(PathSave.PathMatrix);
            chosenMatrix = new Transform[rows, columns];
            GameObject oldGroup = Instantiate(new GameObject("oldBlock"), blockInMap);
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    if (data.saveChosenMatrix[r, c])
                    {
                        GameObject b = Instantiate(baseBlock, mapMatrix[r, c].position, Quaternion.identity, oldGroup.transform);
                        chosenMatrix[r, c] = b.transform;
                    }
                }
            }
            for (int i = 0; i < 3; i++)
            {

            }
        }
        // Load Spawner's Child
        SpawnGroupBlockAgain();
        // Load current score
        if (PlayerPrefs.HasKey(SaveKey.score))
        {
            score = PlayerPrefs.GetInt(SaveKey.score);
            txtScore.text = score.ToString();
        }
        else
        {
            PlayerPrefs.SetInt(SaveKey.score, 0);
            score = PlayerPrefs.GetInt(SaveKey.score);
            txtScore.text = score.ToString();
        }
        // Load highest Score
        if (PlayerPrefs.HasKey(SaveKey.highScore))
        {
            txtHighScore.text = PlayerPrefs.GetInt(SaveKey.highScore).ToString();
        }
        else
        {
            PlayerPrefs.SetInt(SaveKey.highScore, 0);
            txtHighScore.text = PlayerPrefs.GetInt(SaveKey.highScore).ToString();
        }
        //Load rotate Coin
        if (PlayerPrefs.HasKey(SaveKey.rotateCoin))
        {
            rotateCoin = PlayerPrefs.GetInt(SaveKey.rotateCoin);
            txtRotateCoin.text = rotateCoin.ToString();
        }
        else
        {
            PlayerPrefs.SetInt(SaveKey.rotateCoin, 10);
            rotateCoin = PlayerPrefs.GetInt(SaveKey.rotateCoin);
            txtRotateCoin.text = rotateCoin.ToString();
        }
        //Load Treasure slider
        if (PlayerPrefs.HasKey(SaveKey.maxTreasure))
        {
            sliderTreasure.maxValue = PlayerPrefs.GetFloat(SaveKey.maxTreasure);
        }
        else
        {
            PlayerPrefs.SetFloat(SaveKey.maxTreasure, 250);
            sliderTreasure.maxValue = PlayerPrefs.GetFloat(SaveKey.maxTreasure);
        }
        if (PlayerPrefs.HasKey(SaveKey.currentTreasure))
        {
            sliderTreasure.value = PlayerPrefs.GetFloat(SaveKey.currentTreasure);
        }
        else
        {
            PlayerPrefs.SetFloat(SaveKey.currentTreasure, 0);
            sliderTreasure.value = PlayerPrefs.GetFloat(SaveKey.currentTreasure);
        }
    }
}
