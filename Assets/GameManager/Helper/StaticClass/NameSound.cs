﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NameSound
{
    public const string SoundTest = "SoundTest";
    public const string BG = "BG";
    public const string click = "gc_to_tap";
    public const string blockplace = "blockplace";
    public const string blockwrong = "blockwrong";
    public const string lose = "lose";
    public const string popup = "popup";
    public const string BonusNormal = "BonusNormal";
    public const string sfxPerfect = "sfxPerfect";
    public const string NewHighScore = "NewHighScore";
    public const string Backup = "Backup";
    public const string GotTreasure = "GotTreasure";
}
