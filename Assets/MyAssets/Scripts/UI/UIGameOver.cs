﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIGameOver : UIController
{
    public Text txtScore;
    public Text txtHighScore;
    public GameObject newBest;
    private bool canTap;

    public override void Show(bool value)
    {
        if (value)
        {
            // if (GameController.ins.isStop) return; 
            GameController.ins.SaveGame();
            GameController.ins.isStop = true;
            canTap = false;
            transform.GetChild(0).localScale = Vector3.zero;
            newBest.transform.localScale = Vector3.zero;
            gameObject.SetActive(value);
            gameObject.transform.GetChild(0).DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
               {
                   if ((txtScore.text != txtHighScore.text))
                   {
                       canTap = true;
                       AudioManager.ins.PlayOther(NameSound.popup);
                   }
                   gameObject.transform.GetChild(0).DOScale(new Vector3(1f, 1f, 1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
                   {
                       if (txtScore.text == txtHighScore.text)
                       {
                           AudioManager.ins.PlayOther(NameSound.NewHighScore);
                           newBest.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.25f).SetEase(Ease.InOutSine).OnComplete(() =>
                           {
                               newBest.transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.InOutSine).OnComplete(() =>
                               {
                                   newBest.transform.DOScale(Vector3.one, 2f).SetEase(Ease.InOutSine).OnComplete(() =>
                                    {
                                        newBest.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.25f).SetEase(Ease.InOutSine).OnComplete(() =>
                                            {
                                                newBest.transform.DOScale(Vector3.zero, 0.25f).SetEase(Ease.InOutSine);
                                                canTap = true;
                                            });
                                    });
                               });
                           });
                       }
                   });
               });
        }
        else
        {
            gameObject.SetActive(value);
        }
    }

    public void BtnHome()
    {
        if (!canTap) return;
        AudioManager.ins.PlayOther(NameSound.click);
        GameController.ins.isStop = false;
        transform.GetChild(0).DOScale(new Vector3(0f, 0f, 0f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                gameObject.SetActive(false);
                UIManager.ins.ShowUi(MenuUi.Home);
                UIManager.ins.ShowPopup(MenuPopup.None);
                transform.GetChild(0).DOScale(Vector3.one, 0);
            });
    }

    public void BtnRestart()
    {
        if (!canTap) return;
        AudioManager.ins.PlayOther(NameSound.click);
        GameController.ins.isStop = false;
        transform.GetChild(0).DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
           {
               GameController.ins.SpawnBlock();
               transform.GetChild(0).DOScale(new Vector3(0f, 0f, 0f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
               {
                   UIManager.ins.ShowPopup(MenuPopup.None);
                   transform.localScale = Vector3.one;
                   GameController.ins.SaveGame();
               });
           });
    }

    public void BtnRate()
    {
        if (!canTap) return;
        AudioManager.ins.PlayOther(NameSound.click);
        ManagerAds.Ins.RateApp();
    }

    public void BtnShare()
    {
        if (!canTap) return;
        AudioManager.ins.PlayOther(NameSound.click);
        ManagerAds.Ins.ShareApp();
    }
}
