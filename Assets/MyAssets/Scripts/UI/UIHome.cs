﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIHome : UIController
{
    public override void Show(bool value)
    {
        if (value)
        {
            gameObject.SetActive(value);
            transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    transform.DOScale(new Vector3(1, 1, 1), 0.3f).SetEase(Ease.InOutSine);
                    GameController.ins.isStop = true;
                });
        }
        else
        {
            gameObject.SetActive(value);
        }
    }

    public void BtnPlay()
    {
        if (!GameController.ins.canTap) return;
        AudioManager.ins.PlayOther(NameSound.click);
        GameController.ins.isStop = false;
        transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
        {
            transform.DOScale(new Vector3(0f, 0f, 0f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                UIManager.ins.ShowUi(MenuUi.GamePlay);
                GameController.ins.SpawnBlock();
            });
        });
    }

    public void BtnSetting()
    {
        if (!GameController.ins.canTap) return;
        AudioManager.ins.PlayOther(NameSound.click);
        UIManager.ins.ShowPopup(MenuPopup.Setting);
    }

    public void BtnWatchAds()
    {
        if (!GameController.ins.canTap) return;
        AudioManager.ins.PlayOther(NameSound.click);
        UIManager.ins.ShowPopup(MenuPopup.WatchAds);
    }
}
