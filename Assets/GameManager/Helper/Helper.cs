﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public static class Helper 
{
    /// <summary>
    /// Find child in <see cref="Transform"/>
    /// </summary>
    /// <param name="t">Trans need to find child</param>
    /// <param name="list">List all child of transform</param>
    private static void FindAllChild(Transform t, ref List<Object> list)
    {
        list.Add(t.gameObject);
        foreach (Transform child in t)
            FindAllChild(child, ref list);
    }
    
    public static void SetSizeFollowWidth(this Image img, int maxWidth)
    {
        if (img.sprite == null)
            return;
        var sprite = img.sprite;
        float aspect = sprite.bounds.size.y / sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(maxWidth, maxWidth * aspect);
    }

    public static void SetSizeFollowHeight(this Image img, int maxHeight)
    {
        if (img.sprite == null)
            return;
        var sprite = img.sprite;
        float aspect = sprite.bounds.size.y / sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(maxHeight / aspect, maxHeight);
    }
    
    public static Vector3 GetSizeScaleOfSprite(SpriteRenderer sprite)
    {
        Vector3 sizeScale;
        var     size = Camera.main.orthographicSize;

        var worldScreenHeight = size                              * 2.0;
        var worldScreenWidth  = worldScreenHeight / Screen.height * Screen.width;

        var boxSize = sprite.sprite.bounds.size.x;
        sizeScale = ((float)(worldScreenWidth / size) / boxSize) * Vector3.one;

        return sizeScale;
    }
    
    // Clone List
    public static IList<T> Clone<T>(this IList<T> listToClone) where T: ICloneable
    {
        return listToClone.Select(item => (T)item.Clone()).ToList();
    }
    
    public static void Hide(this MonoBehaviour m)
    {
        m.gameObject.SetActive(false);
    }
    
    public static void Hide(this GameObject m)
    {
        m.gameObject.SetActive(false);
    }
    
    public static void Hide(this Transform m)
    {
        m.gameObject.SetActive(false);
    }

    public static void Show(this MonoBehaviour m)
    {
        m.gameObject.SetActive(true);
    }
    
    public static void Show(this GameObject m)
    {
        m.gameObject.SetActive(true);
    }
    
    public static void Show(this Transform m)
    {
        m.gameObject.SetActive(true);
    }
    /// <summary>
    /// Get angle form direction
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    public static float Angle(Vector3 dir)
    {
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        return angle;
    }
    /// <summary>
    /// Get angle two point
    /// </summary>
    /// <param name="t1"></param>
    /// <param name="t2"></param>
    /// <returns></returns>
    public static float Angle(Transform t1, Transform t2)
    {
        var dir = (t1.position - t2.position).normalized;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        return angle;
    }

    public static T Cast<T>(this MonoBehaviour m) where T : class
    {
        return m.GetComponent<T>();
    }
    
    public static T Cast<T>(this GameObject m) where T : class
    {
        return m.GetComponent<T>();
    }
    
    public static T Cast<T>(this Transform m) where T : class
    {
        return m.GetComponent<T>();
    }
}
