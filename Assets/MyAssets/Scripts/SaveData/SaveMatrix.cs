﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveMatrix
{
    public bool[,] saveChosenMatrix;
    public int[] idGroup;
    public float[] angle;
    public SaveMatrix(GameController gameController)
    {
        saveChosenMatrix = new bool[GameController.ins.rows, GameController.ins.columns];
        for (int i = 0; i < saveChosenMatrix.GetLength(0); i++)
        {
            for (int x = 0; x < saveChosenMatrix.GetLength(1); x++)
            {
                if (gameController.chosenMatrix[i, x] != null)
                {
                    saveChosenMatrix[i, x] = true;
                }
                else
                {
                    saveChosenMatrix[i, x] = false;
                }
            }
        }
    }
}
