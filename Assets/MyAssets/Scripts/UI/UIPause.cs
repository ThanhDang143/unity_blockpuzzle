﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIPause : UIController
{
    public Image btnSound;
    public Image btnVibrate;
    public Sprite[] spriteOnOff;

    public override void Show(bool value)
    {
        if (value)
        {
            if (GameController.ins.isStop) return;
            GameController.ins.SaveGame();
            GameController.ins.isStop = true;
            transform.GetChild(0).DOScale(Vector3.zero, 0);
            gameObject.SetActive(value);
            AudioManager.ins.PlayOther(NameSound.popup);
            gameObject.transform.GetChild(0).DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
               {
                   gameObject.transform.GetChild(0).DOScale(new Vector3(1f, 1f, 1f), 0.3f).SetEase(Ease.InOutSine);
               });
        }
        else
        {
            gameObject.SetActive(value);
        }
    }

    public void BtnHome()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        GameController.ins.SaveGame();
        GameController.ins.isStop = false;
        transform.GetChild(0).DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
           {
               transform.GetChild(0).DOScale(new Vector3(0f, 0f, 0f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
                   {
                       UIManager.ins.ShowUi(MenuUi.Home);
                       UIManager.ins.ShowPopup(MenuPopup.None);
                       transform.GetChild(0).DOScale(Vector3.one, 0);
                   });
           });
    }

    public void BtnContinue()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        GameController.ins.SaveGame();
        GameController.ins.isStop = false;
        transform.GetChild(0).DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
           {
               transform.GetChild(0).DOScale(new Vector3(0f, 0f, 0f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
               {
                   UIManager.ins.ShowPopup(MenuPopup.None);
                   transform.GetChild(0).DOScale(Vector3.one, 0);
               });
           });
    }

    public void BtnRestart()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        GameController.ins.score = 0;
        UIManager.ins.gameplay.txtScore.text = GameController.ins.score.ToString();
        GameController.ins.SetTreasure(100);
        GameController.ins.isStop = false;
        foreach (Transform child in GameController.ins.spawner)
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in GameController.ins.frameBackup)
        {
            Destroy(child.gameObject);
        }
        for (int r = 0; r < GameController.ins.rows; r++)
        {
            for (int c = 0; c < GameController.ins.columns; c++)
            {
                if (GameController.ins.chosenMatrix[r, c] != null)
                {
                    Destroy(GameController.ins.chosenMatrix[r, c].gameObject);
                }
            }
        }
        transform.GetChild(0).DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
           {
               GameController.ins.SpawnBlock();
               transform.GetChild(0).DOScale(new Vector3(0f, 0f, 0f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
               {
                   UIManager.ins.ShowPopup(MenuPopup.None);
                   transform.localScale = Vector3.one;
                   GameController.ins.SaveGame();
               });
           });
    }

    public void BtnSound()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        if (PlayerPrefs.GetInt(SaveKey.sound) == 1)
        {
            PlayerPrefs.SetInt(SaveKey.sound, 0);
            btnSound.sprite = spriteOnOff[PlayerPrefs.GetInt(SaveKey.sound)];
            UIManager.ins.setting.btnSound.sprite = spriteOnOff[PlayerPrefs.GetInt(SaveKey.sound)];
        }
        else
        {
            PlayerPrefs.SetInt(SaveKey.sound, 1);
            btnSound.sprite = spriteOnOff[PlayerPrefs.GetInt(SaveKey.sound)];
            UIManager.ins.setting.btnSound.sprite = spriteOnOff[PlayerPrefs.GetInt(SaveKey.sound)];
        }

        try
        {
            foreach (AudioSource s in AudioManager.ins.audioSourceOncePlay)
            {
                s.volume = PlayerPrefs.GetInt(SaveKey.sound);
            }
            AudioManager.ins.sourceBackGround.volume = PlayerPrefs.GetInt(SaveKey.sound);
            AudioManager.ins.sourceClick.volume = PlayerPrefs.GetInt(SaveKey.sound);
        }
        catch (System.Exception)
        {

        }
    }

    public void BtnVibrate()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        if (PlayerPrefs.GetInt(SaveKey.vibrate) == 1)
        {
            PlayerPrefs.SetInt(SaveKey.vibrate, 0);
            btnVibrate.sprite = spriteOnOff[PlayerPrefs.GetInt(SaveKey.vibrate)];
            UIManager.ins.setting.btnVibrate.sprite = spriteOnOff[PlayerPrefs.GetInt(SaveKey.vibrate)];
        }
        else
        {
            PlayerPrefs.SetInt(SaveKey.vibrate, 1);
            btnVibrate.sprite = spriteOnOff[PlayerPrefs.GetInt(SaveKey.vibrate)];
            UIManager.ins.setting.btnVibrate.sprite = spriteOnOff[PlayerPrefs.GetInt(SaveKey.vibrate)];

        }
    }

    public void BtnRate()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        ManagerAds.Ins.RateApp();
    }

    public void BtnShare()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        ManagerAds.Ins.ShareApp();
    }
}
