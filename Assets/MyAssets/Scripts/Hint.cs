﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Hint : MonoBehaviour
{
    public static Hint ins;
    public float timeToHint;

    private void Start()
    {
        ins = this;
        timeToHint = 0;
    }

    private void Update()
    {
        if (GameController.ins.isStop) return;
        timeToHint += Time.deltaTime;
        if (timeToHint > 20)
        {
            Hintt(GameController.ins.spawner.transform);
            timeToHint = 0;
        }
    }

    public void Hintt(Transform parent)
    {
        for (int index = 0; index < parent.childCount; index++)
        {
            GameObject checker = Instantiate(parent.GetChild(index).gameObject, new Vector3(-20, -20, -20), parent.GetChild(index).rotation);
            checker.transform.localScale = Vector3.one;
            checker.tag = "Hinter";
            foreach (Transform c in checker.transform)
            {
                c.GetComponent<SpriteRenderer>().color = Color.green;
                c.GetComponent<SpriteRenderer>().DOFade(0.35f, 0f);
            }
            for (int r = 0; r < GameController.ins.rows; r++)
            {
                for (int c = 0; c < GameController.ins.columns; c++)
                {
                    for (int i = 1; i < 5; i++)
                    {
                        GameController.ins.ChooseCheckPos(i, checker.transform, r, c);
                        if (checker.GetComponent<GroupBlock>().CanDrop())
                        {
                            // Destroy(checker);
                            foreach (Transform child in checker.transform)
                            {
                                checker.GetComponent<GroupBlock>().MoveToMapMatrix(child);
                            }
                            return;
                        }
                    }
                }
            }
            Destroy(checker);
            // return;
        }
    }

    public void DeleteHint()
    {
        if (GameObject.FindGameObjectsWithTag("Hinter") != null)
        {
            foreach (GameObject child in GameObject.FindGameObjectsWithTag("Hinter"))
            {
                Destroy(child);
            }
        }
    }
}
