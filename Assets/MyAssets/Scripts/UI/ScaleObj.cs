﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleObj : MonoBehaviour
{
    [SerializeField] private float baseWidth = 1280f;
    [SerializeField] private float baseHeight = 720f;

    //[SerializeField] private RectTransform rect;
    public float ratio;

    private void Awake()
    {
        var w = baseWidth / Screen.width;
        var h = baseHeight / Screen.height;
        ratio = w / h;
        transform.localScale = Vector3.one * ratio;
    }

}
