﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class UIWatchAds : UIController
{
    public override void Show(bool value)
    {
        if (value)
        {
            GameController.ins.canTap = false;
            GameController.ins.isStop = true;
            transform.GetChild(0).DOScale(Vector3.zero, 0);
            gameObject.SetActive(value);
            AudioManager.ins.PlayOther(NameSound.popup);
            gameObject.transform.GetChild(0).DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
               {
                   gameObject.transform.GetChild(0).DOScale(new Vector3(1f, 1f, 1f), 0.3f).SetEase(Ease.InOutSine);
               });
        }
        else
        {
            gameObject.SetActive(value);
        }
    }

    public void BtnYes()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        ManagerAds.Ins.ShowRewardedVideo((s) =>
        {
            if (s)
            {
                GameController.ins.rotateCoin += 15;
                UIManager.ins.gameplay.txtRotateCoin.text = GameController.ins.rotateCoin.ToString();
                UIManager.ins.ShowPopup(MenuPopup.None);
            }
        });
        GameController.ins.canTap = true;
        GameController.ins.isStop = false;

    }

    public void BtnNo()
    {
        AudioManager.ins.PlayOther(NameSound.click);
        transform.GetChild(0).DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
        {
            transform.GetChild(0).DOScale(new Vector3(0f, 0f, 0f), 0.3f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                UIManager.ins.ShowPopup(MenuPopup.None);
                GameController.ins.canTap = true;
                GameController.ins.isStop = false;
            });
        });
    }
}
