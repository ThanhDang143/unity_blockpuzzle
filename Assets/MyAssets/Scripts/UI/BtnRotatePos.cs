﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnRotatePos : MonoBehaviour
{
    public Vector3 delta;
    private void Start()
    {
        transform.position = GameController.ins.spawner.position - delta;
    }
}
