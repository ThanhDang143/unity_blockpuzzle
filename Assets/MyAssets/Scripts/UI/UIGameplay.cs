﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIGameplay : UIController
{
    public GameObject btnRotate;
    public Sprite[] spritesBtnRotation;
    public Text txtScore;
    public Text txtHighScore;
    public Text txtRotateCoin;
    public ParticleSystem chestEffect;
    public Image imgChest;
    public Sprite[] spriteChest;
    public Slider sliderTreasure;
    public ParticleSystem coinExplosion;

    public override void Show(bool value)
    {
        base.Show(value);
        if (value)
        {
            GameController.ins.isStop = false;
            chestEffect.Stop();
            imgChest.sprite = spriteChest[0];
            imgChest.SetNativeSize();
            btnRotate.GetComponent<Image>().sprite = spritesBtnRotation[0];
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f).SetEase(Ease.InOutSine);
        }
    }

    public void BtnPause()
    {
        if (GameController.ins.isStop)
        {
            return;
        }
        AudioManager.ins.PlayOther(NameSound.click);
        UIManager.ins.ShowPopup(MenuPopup.Pause);
    }

    public void BtnRotate()
    {
        if (GameController.ins.isStop) return;

        AudioManager.ins.PlayOther(NameSound.click);
        if (GameController.ins.canRotate)
        {
            GameController.ins.canRotate = false;
            btnRotate.GetComponent<Image>().sprite = spritesBtnRotation[0];
        }
        else if (!GameController.ins.canRotate)
        {
            GameController.ins.canRotate = true;
            btnRotate.GetComponent<Image>().sprite = spritesBtnRotation[1];
        }
    }

    public void BtnBuyCoin()
    {
        if (GameController.ins.isStop) return;
        AudioManager.ins.PlayOther(NameSound.click);
        UIManager.ins.ShowPopup(MenuPopup.WatchAds);
    }

}
