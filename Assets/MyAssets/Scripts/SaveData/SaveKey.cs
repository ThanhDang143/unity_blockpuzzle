﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveKey
{
    public static string sound = "sound";
    public static string vibrate = "vibrate";
    public static string score = "score";
    public static string highScore = "highScore";
    public static string rotateCoin = "rotateCoin";
    public static string maxTreasure = "maxTreasure";
    public static string currentTreasure = "currentTreasure";
    public static string spawn = "blockinspawn";
    public static string spawnAngle = "eblockinspawn";
    public static string backup = "blockinbackup";
    public static string backupAngle = "eblockinbackup";

}
