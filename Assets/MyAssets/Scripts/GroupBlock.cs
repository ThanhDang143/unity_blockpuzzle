﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GroupBlock : MonoBehaviour
{
    private float holdTime;
    private GameObject tempObj;
    public Vector3 initialPos;
    public int id;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {

        }
    }

    private void OnMouseDown()
    {
        if (GameController.ins.isStop) return;
        SetLayer(100);
        tempObj = Instantiate(gameObject, transform.position, transform.rotation);
        tempObj.SetActive(false);
        tempObj.transform.DOScale(Vector3.one, 0);
        foreach (Transform child in tempObj.transform)
        {
            child.GetComponent<SpriteRenderer>().sortingOrder = transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder - 1;
            child.GetComponent<SpriteRenderer>().DOFade(0.5f, 0);
        }
    }

    private void OnMouseDrag()
    {
        if (GameController.ins.isStop) return;
        Hint.ins.timeToHint = 0;
        if (!GameController.ins.canRotate)
        {
            transform.DOScale(Vector3.one, 0.25f);
            transform.DOMove(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0) + Vector3.up, 0.1f);
            ShowDroppableBlock();
        }
        else if (GameController.ins.canRotate)
        {
            holdTime += Time.deltaTime;
            if (holdTime > 0.2f)
            {
                transform.DOScale(Vector3.one, 0.25f);
                transform.DOMove(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 0) + Vector3.up, 0.25f - holdTime + 0.2f);
                ShowDroppableBlock();
            }
        }
    }

    private void OnMouseUp()
    {
        if (GameController.ins.isStop) return;
        Hint.ins.DeleteHint();
        GetComponent<BoxCollider2D>().enabled = false;
        if (!GameController.ins.canRotate)
        {
            DropToMatrix();
        }
        else if (GameController.ins.canRotate)
        {
            if (holdTime > 0.2f)
            {
                DropToMatrix();
            }
            else
            {
                StartCoroutine(IERotateBlock());
            }
            holdTime = 0;
        }
        Destroy(tempObj);
        GameController.ins.DestroyEmptyGroupBlock();
        GameController.ins.SpawnBlock();
        GameController.ins.EndGame();
    }

    private IEnumerator IERotateBlock()
    {
        if (GameController.ins.rotateCoin <= 0)
        {
            yield break;
        }
        GameController.ins.rotateCoin -= 1;
        UIManager.ins.gameplay.txtRotateCoin.text = GameController.ins.rotateCoin.ToString();
        float transformZ = transform.eulerAngles.z;
        float childZ = transform.GetChild(0).eulerAngles.z;
        transform.DORotate(new Vector3(0, 0, (transformZ + 90)), 0.25f);
        foreach (Transform child in transform)
        {
            child.transform.DORotate(new Vector3(0, 0, childZ), 0.25f);
        }
        transform.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(0.25f);
        transform.GetComponent<BoxCollider2D>().enabled = true;
    }

    private void ShowDroppableBlock()
    {
        if (CanDrop())
        {
            tempObj.SetActive(true);

            for (int i = 0; i < transform.childCount; i++)
            {
                tempObj.transform.GetChild(i).position = GameController.ins.mapMatrix[MathfRoundToInt(transform.GetChild(i).position.y - GameController.ins.map.position.y), MathfRoundToInt(transform.GetChild(i).position.x)].position;
            }
        }
        else
        {
            tempObj.SetActive(false);
        }
    }

    public void DropToMatrix()
    {
        if (Mathf.Abs(transform.position.x - GameController.ins.frameBackup.position.x) <= 1.5f && Mathf.Abs(transform.position.y - GameController.ins.frameBackup.position.y) <= 1.5f && GameController.ins.frameBackup.childCount < 1)
        {
            AudioManager.ins.PlayOther(NameSound.Backup);
            SetLayer(0);
            transform.SetParent(GameController.ins.frameBackup);
            transform.DOMove(GameController.ins.frameBackup.position, 0.5f).OnComplete(() =>
            {
                GetComponent<BoxCollider2D>().enabled = true;
                GameController.ins.SaveGame();
            });
            transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 0.5f);
            initialPos = GameController.ins.frameBackup.position;
        }
        else
        {
            if (!CanDrop())
            {
                AudioManager.ins.PlayOther(NameSound.blockwrong);
                transform.DOScale(new Vector3(0.35f, 0.35f, 0.35f), 0.5f);
                transform.DOMove(initialPos, 0.5f).OnComplete(() =>
                {
                    SetLayer(0);
                    GetComponent<BoxCollider2D>().enabled = true;
                });
                return;
            }
            AudioManager.ins.PlayOther(NameSound.blockplace);
            GameController.ins.UpScore(transform.childCount);
            foreach (Transform child in transform)
            {
                AddToChosenMatrix(child);
                MoveToMapMatrix(child);
            }
            GameController.ins.Bonus();
            transform.SetParent(GameController.ins.blockInMap);
        }
    }

    public bool CanDrop()
    {
        // Check block đã được kéo vào trong map
        // Check vị trí drop có còn trống không
        foreach (Transform child in transform)
        {
            if (MathfRoundToInt(child.position.x - GameController.ins.map.position.x) < 0 || MathfRoundToInt(child.position.x - GameController.ins.map.position.x) > GameController.ins.rows - 1
            || MathfRoundToInt(child.position.y - GameController.ins.map.position.y) < 0 || MathfRoundToInt(child.position.y - GameController.ins.map.position.y) > GameController.ins.columns - 1
            || GameController.ins.chosenMatrix[MathfRoundToInt(child.position.y - GameController.ins.map.position.y), MathfRoundToInt(child.position.x - GameController.ins.map.position.x)] != null)
            {
                return false;
            }
        }
        return true;
    }

    private int MathfRoundToInt(float number)
    {
        return Mathf.RoundToInt((number - (GameController.ins.startX)) / GameController.ins.distance);
    }

    private void SetLayer(int layer)
    {
        foreach (Transform child in transform)
        {
            child.GetComponent<SpriteRenderer>().sortingOrder = layer;
        }
    }

    private void AddToChosenMatrix(Transform block)
    {
        GameController.ins.chosenMatrix[MathfRoundToInt(block.position.y - GameController.ins.map.position.y), MathfRoundToInt(block.position.x - GameController.ins.map.position.x)] = block;
    }

    public void MoveToMapMatrix(Transform block)
    {
        GameController.ins.CheckColumn(MathfRoundToInt(block.position.x - GameController.ins.map.position.x));
        GameController.ins.CheckRow(MathfRoundToInt(block.position.y - GameController.ins.map.position.y));
        block.DOMove(GameController.ins.mapMatrix[MathfRoundToInt(block.position.y - GameController.ins.map.position.y), MathfRoundToInt(block.position.x - GameController.ins.map.position.x)].position, 0.125f).OnComplete(() =>
           {
               SetLayer(0);
               GameController.ins.ClearCountList();
           });
    }
}
