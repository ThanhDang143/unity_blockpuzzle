﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager ins;
    public UIHome home;
    public UIGameplay gameplay;
    public UIPause pause;
    public UIGameOver gameover;
    public UISetting setting;
    public UIWatchAds watchAds;
    public ParticleSystem[] effect;

    private void Awake()
    {
        ins = this;
        ShowUi(MenuUi.Home);
        ShowPopup(MenuPopup.None);
    }

    public void ShowUi(MenuUi menuUi)
    {
        home.Show(menuUi == MenuUi.Home);
        gameplay.Show(menuUi == MenuUi.GamePlay);
    }

    public void ShowPopup(MenuPopup popup)
    {
        pause.Show(popup == MenuPopup.Pause);
        gameover.Show(popup == MenuPopup.GameOver);
        setting.Show(popup == MenuPopup.Setting);
        watchAds.Show(popup == MenuPopup.WatchAds);
        if (popup != MenuPopup.None)
        {
            foreach (ParticleSystem e in effect)
            {
                e.Stop();
            }
        }
        else
        {
            effect[0].Play();
            if (gameplay.sliderTreasure.value >= 0.75 * gameplay.sliderTreasure.maxValue && effect[1].isStopped)
            {
                effect[1].Play();
            }
        }
    }
}

public enum MenuUi
{
    None,
    Home,
    GamePlay,
}

public enum MenuPopup
{
    None,
    Pause,
    GameOver,
    Setting,
    WatchAds,
}

