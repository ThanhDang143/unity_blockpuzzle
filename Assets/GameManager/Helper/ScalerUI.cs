﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScalerUI : MonoBehaviour
{
    [SerializeField] private float baseWidth = 1280f;
    [SerializeField] private float baseHeight = 720f;
    [SerializeField] private CanvasScaler scaler;
    //[SerializeField] private RectTransform rect;
    public float ratio;

    private void Awake()
    {
        scaler = this.Cast<CanvasScaler>();
        if(scaler == null) return;
        var w = baseWidth / Screen.width;
        var h = baseHeight / Screen.height;
        ratio = h / w;
        ratio = ratio >= 1 ? 1 : 0;
        scaler.matchWidthOrHeight = ratio;
    }
}
