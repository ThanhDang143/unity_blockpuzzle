﻿using System;
using System.Collections;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager ins;

    public AudioClip[] audioClips;
    public AudioSource sourceBackGround;
    public AudioSource sourceClick;
    public AudioSource[] audioSourceOncePlay;
    public Sound[] sounds;
    public Sound[] soundsBackGround;

    void Awake()
    {
        if (ins != null)
        {
            Destroy(gameObject);
        }
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
        AudioManager.ins.PlayBackGround(NameSound.BG);
    }

    [ContextMenu("CreateSound")]
    public void CreateSound()
    {
        sounds = new Sound[audioClips.Length];
        for (int i = 0; i < sounds.Length; i++)
        {
            //sounds[i] = new Sound(audioClips[i].name,audioClips[i],false);
            sounds[i] = new Sound();
            sounds[i].name = audioClips[i].name;
            sounds[i].clip = audioClips[i];
            sounds[i].volume = 2;
            sounds[i].pitch = 1;
        }
    }

    public void PlayOther(string nameSound)
    {
        if (PlayerPrefs.GetInt(KeyPre.Sound, 1) == 0) return;
        foreach (var t in sounds)
        {
            if (t.name.Equals(nameSound))
            {
                var index = 0;
                while (audioSourceOncePlay[index].isPlaying)
                {
                    index++;
                    if (index >= audioSourceOncePlay.Length)
                    {
                        var s = Instantiate(audioSourceOncePlay[0]);
                        s.loop = t.loop;
                        s.clip = t.clip;
                        s.Play();
                        Destroy(s.transform, s.clip.length);
                        return;
                    }
                }

                audioSourceOncePlay[index].loop = t.loop;
                audioSourceOncePlay[index].clip = t.clip;
                audioSourceOncePlay[index].Play();
                return;
            }
        }

        Debug.LogWarning("Not Found " + nameSound);
    }

    public void StopOther(string nameSound)
    {
        for (int i = 0; i < audioSourceOncePlay.Length; i++)
        {
            if (audioSourceOncePlay[i].clip.name.Equals(nameSound))
            {
                audioSourceOncePlay[i].Stop();
                return;
            }
        }
    }

    public void StopAllSound()
    {
        for (int i = 0; i < audioSourceOncePlay.Length; i++)
        {
            audioSourceOncePlay[i].Stop();
        }
    }

    public void Click()
    {
        if (PlayerPrefs.GetInt(KeyPre.Sound, 1) == 0) return;
        sourceClick.Play();
    }

    public void PlayBackGround(string names)
    {
        if (PlayerPrefs.GetInt(KeyPre.Music, 1) == 0) return;
        if (sourceBackGround.clip != null && names.Equals(sourceBackGround.clip.name)) return;
        var s = Array.Find(soundsBackGround, sound => sound.name == names);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + names + " not found");
            return;
        }

        sourceBackGround.loop = true;
        sourceBackGround.clip = s.clip;
        sourceBackGround.Play();
        //s.source.Play();
    }

    public void StopBackGround()
    {
        sourceBackGround.Stop();
    }

    public void ContinueBg()
    {
        if (sourceBackGround.clip != null)
        {
            StopAllCoroutines();
            sourceBackGround.Play();
        }
    }

    public void PauseBg(bool win)
    {
        StartCoroutine(IePause(win));
    }

    IEnumerator IePause(bool win)
    {
        sourceBackGround.Pause();
        if (win)
        {
            yield return new WaitForSeconds(2.7f);
        }
        else
        {
            yield return new WaitForSeconds(4f);
        }

        sourceBackGround.Play();
    }

    public void ClickButton()
    {
        if (PlayerPrefs.GetInt(KeyPre.Sound, 1) == 0) return;
        sourceClick.Play();
    }
}